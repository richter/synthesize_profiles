#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 15:20:18 2022

@author: richter
"""
import pandas as pd
import datetime

import sys
sys.path.append('./')
import get_synthesized_stratigraphy_variables

#%%
print( get_synthesized_stratigraphy_variables.loadProfile.__doc__ )

filename = 'input/WFJ2.pro'
file_out_daily = 'output_extracted/WFJ2_sythesized_profile.csv'

#Daily output:
df = get_synthesized_stratigraphy_variables.loadProfile(filename, filename_output=file_out_daily, daily = True, write_df=True)

#Output, if you don't want to write a file, delete key write_df=True
file_out = 'output_extracted/WFJ2_sythesized_profile.csv'
df = get_synthesized_stratigraphy_variables.loadProfile(filename, filename_output=file_out, write_df=True)
#%%


file_out_detailed = 'output_extracted/WFJ2_sythesized_profile_detailed.csv'
df = get_synthesized_stratigraphy_variables.loadProfile(filename, filename_output=file_out_detailed, write_df=True,
    structure_depths = [10,20,50,100], 
    structure_variables = ['lwc','temperature', 'swe', 'density', 'hand_hardness', 'perc_pers', 'perc_crust'],
    nwl=2, deepestwl=True, addallwlinfo = True, 
    penetration_depth_properties = True)


#Print synthezied variables for one timestamp:
ts=datetime.datetime(2022, 2, 1, 12)
print(df.loc[ts])

#%%
#Print synthezied variables for one timestamp from file:
df_fromfile = pd.read_csv(file_out, index_col=0, parse_dates=True)
ts=datetime.datetime(2022, 2, 1, 12)
print(df_fromfile.loc[ts])


